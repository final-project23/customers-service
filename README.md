# customers-service

# Nama Aplikasi = Perpustakaan

# Rest API = - flask = - flask digunakan karena lebih ringan dari rest API yg lain
#                      - dapat mengatur routing
#                      - akses method post
#                      - akses parameter get

#            - flask_jwt_extended = - untuk mengatur pilot dari email user
#                                   - sebagai security aplikasi 

# @jwt_required() = mengatur pilot dari email user
# if dbresult is not None: = mengambil user lebih dari satu
# @app.route() = sebagai routing aplikasi (get, post)
# json.dumps = merubah id yg d pilih menjadi sebuah json
# user = {} = di ambil dari inner join pada bagian models
# return film_data.json() = untuk membalikan ke json dengan cara mengambil data menggunakan restApi dari getFilmById

# pada bagian ini tidak jauh beda, disebabkan saya menggunakan mysql pada dua service tersebut.