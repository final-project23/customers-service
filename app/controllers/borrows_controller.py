from app.models.borrows_model import database
from app.models.customers_model import database as cust_db
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests

mysqldb = database()
cust_db = cust_db()

@jwt_required() # bisa masuk kedalam semua dalam hal jwt # penggunaan untuk mengatur pilot dari email user
def shows():
    # ambil payload kita dari token
    params = get_jwt_identity() #akan membalikan identitas dari jwt
    dbresult = mysqldb.showBorrowByEmail(**params)
    result = []
    if dbresult is not None: # karena usernya lebih dari satu
        for items in dbresult:
            id = json.dumps({"id":items[4]}) # merubah id (items ke 4) yg d pilih menjadi sebuah json
            bookdetails = getBookById(id)
            user = {
                "username": items[0],
                "borrowid": items[1],
                "borrowdate": items[2],
                "bookid": items[4],
                "bookname": items[5],
                "author": bookdetails["pengarang"],
                "releaseyear": bookdetails["tahun_terbit"],
                "genre": bookdetails["genre"]
            } # di ambil dari inner join
            result.append(user)
    else:
        result=dbresult
    return jsonify(result)

@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    user = cust_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id - json.dumps({"id":params["bookid"]})
    bookname = getBookById(id)["nama"]
    params.update(
        {
            "userid":userid,
            "borrowdate":borrowdate,
            "bookname":bookname,
            "isactive":1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def getBookById(data):
    book_data = requests.get(url="http://localhost:8000/bookbyid", data=data)
    return book_data.json() # untuk membalikan ke json # mengambil data dengan restApi dari getBookById